
# ディレクトリ取得
CURRENT_DIR=$(abspath .)
PARENT_DIR=$(abspath ..)
BASE_DIR:=$(abspath $(dir $(lastword $(MAKEFILE_LIST))))
OUT_DIR=$(BASE_DIR)/video/_output
UTILS_DIR=$(BASE_DIR)/utils/scripts

# 環境
IS_BASH_WSL=$(shell bash -c '[[ "$$(cat /proc/sys/kernel/osrelease)" =~ .*[Mm]icrosoft.* ]] && echo 1')

# 実行ファイル
AERENDER= $(UTILS_DIR)/sjis_cmd.sh /c/Program\ Files/Adobe/Adobe\ After\ Effects\ CC\ 2019/Support\ Files/aerender.exe
ifeq ($(IS_BASH_WSL),1)
    AERENDER= $(UTILS_DIR)/sjis_cmd.sh /mnt/c/Program\ Files/Adobe/Adobe\ After\ Effects\ CC\ 2019/Support\ Files/aerender.exe
endif
RENDER_OPTS:= -sound ON -OMtemplate LagarithRGBA -mp -continueOnMissingFootage

BLENDER=/c/Program\ Files/Blender\ Foundation/Blender\ 3.6/blender.exe
ifeq ($(IS_BASH_WSL),1)
    BLENDER= /mnt/c/Program\ Files/Blender\ Foundation/Blender\ 3.6/blender.exe
endif

# コンポ設定
SCENE_NUM=$(shell basename $(PARENT_DIR))
CUT_NUM=$(shell basename $(CURRENT_DIR))
AEP_FILE_NAMES=$(wildcard *.aep)
AEP_FILES=$(addprefix $(CURRENT_DIR)/,$(AEP_FILE_NAMES))
BLEND_FILE_NAMES=$(wildcard *.blend)
BLEND_FILES=$(addprefix $(CURRENT_DIR)/,$(BLEND_FILE_NAMES))
AVI_FILE_NAMES=$(patsubst %.aep,%.avi,$(AEP_FILE_NAMES))
AVI_FILES=$(addprefix $(OUT_DIR)/,$(AVI_FILE_NAMES))
BLENDER_AVI_FILE_NAMES=$(patsubst %.blend,%.avi,$(BLEND_FILE_NAMES))
BLENDER_AVI_FILES=$(addprefix $(OUT_DIR)/,$(BLENDER_AVI_FILE_NAMES))
MATERIAL_FILES=$(shell find $(CURRENT_DIR) -iname '*.psd' | sed 's/ /\\ /g')  $(shell find $(CURRENT_DIR) -iname '*.ai' | sed 's/ /\\ /g') $(shell find $(CURRENT_DIR) -iname '*.png' | sed 's/ /\\ /g') $(shell find $(CURRENT_DIR) -iname '*.blend' | sed 's/ /\\ /g')
COMP_NAME=_output


# エンコード関連ユーティリティのディレクトリ
UTIL_DIR:=$(BASE_DIR)/utils/bin
ENC_UTIL_DIR:=$(BASE_DIR)/utils/enc_utils/bin

# エンコード関連
AVS2WAV:=$(ENC_UTIL_DIR)/avs2wav.exe
AVS2YUV:=$(ENC_UTIL_DIR)/avs2yuv.exe
FFMPEG:=ffmpeg
X264:=$(ENC_UTIL_DIR)/x264_x64.exe
MUXER:=$(ENC_UTIL_DIR)/muxer.exe
REMUXER:=$(ENC_UTIL_DIR)/remuxer.exe

#共通処理
define STANDARD_RENDER
    $(AERENDER) -project "$(shell echo "$<" | xargs $(UTILS_DIR)/winpath )" -comp "$(COMP_NAME)" -output "$(shell echo "$@" | xargs $(UTILS_DIR)/winpath)" $(RENDER_OPTS)
endef

define BLENDER_RENDER
    $(BLENDER) -b "$(shell echo "$<" | xargs $(UTILS_DIR)/winpath )" -P "$(shell echo "$(UTILS_DIR)/blender_forcebake.py" | xargs $(UTILS_DIR)/winpath )" -o "$(shell echo "$@.temp.avi" | xargs $(UTILS_DIR)/winpath)" -x 0 -a
    $(FFMPEG) -i "$@.temp.avi" -codec:v utvideo -y "$@"
    rm "$@.temp.avi"
endef

# ユーザー設定
sinclude $(BASE_DIR)/Makefile.user.inc
