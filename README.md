こうしす！EE #6
=============

**「広報部システム課（隠ぺいと、鉄道員と、精神論 Part4）」**

情報セキュリティ普及啓発アニメ「こうしす！」シリーズ第2弾。
本作品はオープンソース版「こうしす！」を商業配信用に一部改変したものです。

本作品の一部にはフリーではない素材が含まれます。
詳細はLICENSE.txtをご覧ください。


## オープンソース版のリポジトリ

<https://gitlab.com/kosys/kosys-ep03-part4>



## クレジット
### 改変
Copyright (C) 2024 Kyoki Railway LLC.

この変更は基本的にCC-BY 4.0ライセンスのもとに提供されていますが、「nonfree」という名前のフォルダに含まれるファイルはその限りではありません。

### 原作

Copyright (C) 2024 OPAP-JP contributors.

この作品は[CC-BY 4.0](http://creativecommons.org/licenses/by/4.0/)および[CC-BY 2.1 JP](http://creativecommons.org/licenses/by/2.1/jp/)のもとに提供されています。
この作品をご利用の際には、適切なクレジットの表示が必要です。クレジットや著作権についての詳細は <https://opap.jp/contributors> をご覧ください。
