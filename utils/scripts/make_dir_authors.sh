#!/bin/bash


################################################################################
# 定数・変数
################################################################################

export LANG=ja_JP.UTF-8

pushd "$(dirname $0)" > /dev/null
SCRIPTPATH="$(pwd)"
popd > /dev/null

which cygpath > /dev/null
if [ $? -ne 0 ]; then
	REPOSITORY_ROOT="$(git rev-parse --show-toplevel)"
else
	REPOSITORY_ROOT="$(cygpath "$(git rev-parse --show-toplevel)")"
fi

OUTFILE=AUTHORS.txt
TMPFILE=AUTHORS.tmp
MAPFILE=${SCRIPTPATH}/name_map.csv


# 名前マッピング用の連想配列(名前 -> マップ後の名前)
declare -A name_map

################################################################################
# 関数
################################################################################


# 名前マッピング用の連想配列を初期化
function InitNameMap() {

	while IFS='	' read f1 f2
	do
		if [ "$f1" = "" ]; then
			continue
		fi
		if [ "$f2" = "" ]; then
			continue
		fi 
		
		name_map["${f1}"]="${f2}"
		
	done < <(perl -pe 's/^\xEF\xBB\xBF//g; s/\r//' "${MAPFILE}")
}

# 標準入力をマップ後の名前に変換して標準出力に出力する
function MapNames() {

	if [ ${#name_map[@]} -eq 0 ]; then
		InitNameMap
	fi
	
	while read original_name; do
		if [ -v name_map["${original_name}"] ]; then
			echo "${name_map["${original_name}"]}"
		else
			echo "${original_name}"
		fi
	done 
	
	return 0
}

# Gitログから貢献者のリストを取得する
function GetAuthorNamesFromGitLog() {
	local IFS=$'\n'
	
	local names=($( \
		git --no-pager log  --reverse --pretty=format:"%an" . \
			| grep -v -e 'www-data' -e 'system' -e 'guest'  \
			| MapNames \
			| awk '!x[$0]++' \
	))
	
	printf '%s\n' "${names[@]}" 
	return 0
}


# サブディレクトリのAUTHORS.txtから貢献者のリストを取得する
function GetAuthorNamesFromSubDir() {

	local IFS=$'\n'
	local names=($( \
		find . -mindepth 2 -not -wholename '*/_site/*' -type f -iname 'AUTHORS.txt' -print0 \
			| xargs -0 sed -E 's/$/\n/g' \
			| perl -pe 's/^\xEF\xBB\xBF//g; s/\r//; s/\t.*$//g' \
			| sed -E '/^#/d' \
			| MapNames \
			| awk '!x[$0]++' \
	))

	printf '%s\n' "${names[@]}" 
	return 0
}

################################################################################
# メイン
################################################################################

if [ "${REPOSITORY_ROOT}" = "" ]; then
	echo "Gitリポジトリ内ではありません。" >&2
	exit 1
fi


IFS=$'\n'
names_all=()
names_all+=($( \
	GetAuthorNamesFromGitLog \
))
names_all+=($( \
	GetAuthorNamesFromSubDir \
))

echo -n "" > "${TMPFILE}"
if [ "$(pwd)" = "${REPOSITORY_ROOT}" ]; then
	echo "################################################################################" >> "${TMPFILE}"
	echo "# This file is contributors list of this repository. "                            >> "${TMPFILE}"
	echo "# See https://opap.jp/contributors for contributors of common materials." >> "${TMPFILE}"
	echo "#-------------------------------------------------------------------------------" >> "${TMPFILE}"
	echo "# このファイルは、このリポジトリの貢献者リストです。 "                            >> "${TMPFILE}"
	echo "# 共通素材の貢献者等は、https://opap.jp/contributors をご覧ください。 "                            >> "${TMPFILE}"
	echo "################################################################################" >> "${TMPFILE}"
else
	echo "################################################################################" >> "${TMPFILE}"
	echo "# This file is contributors list of this directory and its sub directories. "     >> "${TMPFILE}"
	echo "# See <repository root>/common/AUHTORS.txt for all contributors." >> "${TMPFILE}"
	echo "#-------------------------------------------------------------------------------" >> "${TMPFILE}"
	echo "# このファイルはこのディレクトリおよびサブディレクトリの貢献者リストです。"       >> "${TMPFILE}"
	echo "# すべての貢献者は、<リポジトリルート>/AUTHORS.txtをご覧ください。 "       >> "${TMPFILE}"
	echo "################################################################################" >> "${TMPFILE}"

fi

echo "#"  >> "${TMPFILE}"
echo "# Note: "                                                               >> "${TMPFILE}"
echo "#   - Revision history is available at "                                >> "${TMPFILE}"
echo "#     https://gitlab.com/kosys/kosys-ep03-part4 . "           >> "${TMPFILE}"
echo "#"  >> "${TMPFILE}"
echo "# 注記: "                                                                          >> "${TMPFILE}"
echo "#   - 更新履歴は以下からご覧いただけます。 "                                       >> "${TMPFILE}"
echo "#     https://gitlab.com/kosys/kosys-ep03-part4  "                       >> "${TMPFILE}"
echo "#"  >> "${TMPFILE}"


echo "" >> "${TMPFILE}"
	
printf '%s\n' "${names_all[@]}" \
	| awk '!x[$0]++' \
	| sort \
	>> "${TMPFILE}"


echo "### END AUTO-GENERATED LIST.  LEAVE THIS LINE HERE; SCRIPTS LOOK FOR IT. ###" >> "${TMPFILE}"

if [ -f "${OUTFILE}" ]; then
	grep '^### END AUTO-GENERATED LIST\.' "${OUTFILE}" > /dev/null
	if [ $? -eq 0 ]; then
		sed -e '1,/^### END AUTO-GENERATED LIST\./ d' "${OUTFILE}" | perl -pe 's/^\xEF\xBB\xBF//g; s/\r//' >>  "${TMPFILE}"
	else
		 perl -pe 's/^\xEF\xBB\xBF//g; s/\r//' "${OUTFILE}" >>  "${TMPFILE}"
	fi
fi

perl -pe 's/\n/\r\n/' "${TMPFILE}" > "${OUTFILE}"
rm -f "${TMPFILE}" 

